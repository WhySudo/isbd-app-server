<?php 
define("QUERY_CHECK_LOGIN", "QUERY_CHECK_LOGIN");

define("QUERY_GET_WIZARD", "QUERY_GET_WIZARD");
define("QUERY_GET_WIZARD_BOOKS", "QUERY_GET_WIZARD_BOOKS");
define("QUERY_GET_WIZARD_SKILLS", "QUERY_GET_WIZARD_SKILLS");
define("QUERY_GET_WIZARD_TOURS", "QUERY_GET_WIZARD_TOURS");
define("QUERY_GET_WIZARD_LESSONS", "QUERY_GET_WIZARD_LESSONS");

define("QUERY_GET_TOURS_LIST", "QUERY_GET_TOURS_LIST");
define("QUERY_GET_TOUR_INFO", "QUERY_GET_TOUR_INFO");
define("QUERY_GET_TOUR_WIZARDS", "QUERY_GET_TOUR_WIZARDS");
define("QUERY_TOUR_ENTER", "QUERY_TOUR_ENTER");
define("QUERY_TOUR_EXIT", "QUERY_TOUR_EXIT");
define("QUERY_TOUR_ADD", "QUERY_TOUR_ADD");
define("QUERY_TOUR_SET_WINNER", "QUERY_TOUR_SET_WINNER");
define("QUERY_TOUR_EXCLUDE", "QUERY_TOUR_EXCLUDE");

define("QUERY_GROUP_GET_INFO", "QUERY_GROUP_GET_INFO");
define("QUERY_GROUP_GET_STUDENTS", "QUERY_GROUP_GET_STUDENTS");
define("QUERY_GROUP_GET_LESSONS", "QUERY_GROUP_GET_LESSONS");

define("QUERY_ROOM_GET_INFO", "QUERY_ROOM_GET_INFO");
define("QUERY_STOR_GET_LIST", "QUERY_STOR_GET_INFO");
define("QUERY_ROOM_GET_LIST", "QUERY_ROOM_GET_LIST");


$db_inst = pg_connect("host=localhost port=5432 dbname=studs user=sXXXXXX password=xxxxxx");
pg_set_client_encoding($db_inst, "UTF-8");
pg_prepare($db_inst, QUERY_CHECK_LOGIN, 'SELECT "Wizard"."Id" as "id", "GroupId" as "groupId",case when "Teacher"."Id" is not null then TRUE ELSE FALSE END AS "status" FROM "Wizard" LEFT JOIN "Teacher" ON "Wizard"."Id" = "Teacher"."WizardId" WHERE "Login" = $1 AND "Password" = $2');
pg_prepare($db_inst, QUERY_GET_WIZARD, 'SELECT "Wizard"."Id" AS "id", "Wizard"."Name" as "name", "Wizard"."Age" as "age", "Group"."Name" as "group", "Group"."Id" as "groupid", case when "Teacher"."Id" is not null then TRUE ELSE FALSE END AS "status" FROM "Wizard" LEFT JOIN "Group" ON "Wizard"."GroupId" = "Group"."Id" LEFT JOIN "Teacher" ON "Wizard"."Id" = "Teacher"."WizardId" WHERE "Wizard"."Id" = $1');
pg_prepare($db_inst, QUERY_GET_WIZARD_BOOKS, 'select array_to_json(array_agg(wz."Name")) as "authors", bk."Name" as "name", bk."Condition" as "condition", bk."Listcount" as "pageCount", ls."Name" as "lesson" from "Wizard" wz inner join "BookAuthority" ba On wz."Id" = ba."WizardId" inner join "Book" bk on bk."Id" = ba."BookId" inner join "Lesson" ls on ls."Id" = bk."LessonId" group by bk."Name", bk."Condition", bk."Listcount", ls."Name"  HAVING (select "Name" from "Wizard" where "Id" = $1) = ANY(array_agg(wz."Name"))');
pg_prepare($db_inst, QUERY_GET_WIZARD_SKILLS, 'select sp."Id" as "id", sp."Name" as "name", bk."Name" as "book", sp."Difficulty" as "difficulty", array_to_json(array_agg(pt."Name")) as "potions" from "Wizard" wz inner join "WizardSkill" ws On ws."WizardId" = wz."Id" inner join "Skill" sp on sp."Id" = ws."SkillId" inner join "Book" bk on bk."Id" = sp."BookId" left join "SkillPotion" spt on spt."SkillId" = sp."Id" left join "Potion" pt on spt."PotionId" = pt."Id"  where wz."Id" = $1 group by sp."Id", sp."Name", bk."Name", sp."Difficulty"');

pg_prepare($db_inst, QUERY_GET_WIZARD_TOURS, 'select tm."Id" as "id", tm."Name" as "name", ls."Name" as "subject", count(tp."WizardId") as "numOfParticip", tm."Owner" as "organizerId", (select "Wizard"."Name" from "Wizard" where "Wizard"."Id" = tm."Owner") as "organizer", tm."WinnerWizardId" as "winnerId", (select "Wizard"."Name" from "Wizard" where "Wizard"."Id" = tm."WinnerWizardId") as "winner" from "TournamentParticipant" tp right join "Tournament" tm on tm."Id" = tp."TournamentId" inner join "Lesson" ls on ls."Id" = tm."LessonId" where tp."WizardId" = $1 or tm."Owner" = $1 group by tm."Id", ls."Name"
');
pg_prepare($db_inst, QUERY_GET_TOURS_LIST, 'select tm."Id" as "id", tm."Name" as "name", ls."Name" as "subject", count(tp."WizardId") as "numOfParticip", tm."Owner" as "organizerId", (select "Wizard"."Name" from "Wizard" where "Wizard"."Id" = tm."Owner") as "organizer", tm."WinnerWizardId" as "winnerId", (select "Wizard"."Name" from "Wizard" where "Wizard"."Id" = tm."WinnerWizardId") as "winner" from "TournamentParticipant" tp right join "Tournament" tm on tm."Id" = tp."TournamentId" inner join "Lesson" ls on ls."Id" = tm."LessonId" group by tm."Id", ls."Name"
');

pg_prepare($db_inst, QUERY_GET_TOUR_WIZARDS, 'select wz."Id" as "id", wz."Name" as "name", wz."Age" as "age" from "Wizard" wz inner join "TournamentParticipant" tp on wz."Id" = tp."WizardId" where tp."TournamentId" = $1');
pg_prepare($db_inst, QUERY_GET_TOUR_INFO, 'select tm."Id" as "id", tm."Name" as "name", ls."Name" as "subject", count(tp."WizardId") as "numOfParticip", tm."Owner" as "organizerId", (select "Wizard"."Name" from "Wizard" where "Wizard"."Id" = tm."Owner") as "organizer", tm."WinnerWizardId" as "winnerId", (select "Wizard"."Name" from "Wizard" where "Wizard"."Id" = tm."WinnerWizardId") as "winner" from "TournamentParticipant" tp right join "Tournament" tm on tm."Id" = tp."TournamentId" inner join "Lesson" ls on ls."Id" = tm."LessonId" where tm."Id" = $1 group by tm."Id", ls."Name"
');

pg_prepare($db_inst, QUERY_GET_WIZARD_LESSONS, 'Select ls."Id" as "id", ls."Name" as "name" from "Wizard" wz inner join "Teacher" tch on wz."Id" = tch."WizardId" inner join "StudyPermission" sp on sp."TeacherId" = tch."Id" inner join "Lesson" ls on sp."LessonId" = ls."Id" where wz."Id" = $1');
//$1 - id, $2 - tourid
pg_prepare($db_inst, QUERY_TOUR_ENTER, 'insert into "TournamentParticipant"("WizardId","TournamentId") select $1, $2 from "Tournament" tm where tm."WinnerWizardId" is null and tm."Id" = $2 RETURNING "WizardId","TournamentId"');
//$1 - id, $2 - tourid
pg_prepare($db_inst, QUERY_TOUR_EXIT, 'delete from "TournamentParticipant" tp where tp."WizardId" = $1 and tp."TournamentId" = $2 and EXISTS(select * from "Tournament" where "WinnerWizardId" is null and "Id" = $2) RETURNING *');
//$1 - id, $2 - tourname, $3 tourLessonId
pg_prepare($db_inst, QUERY_TOUR_ADD, 'insert into "Tournament"("Owner","Name","LessonId") values($1, $2, $3) RETURNING "Id"');//TODO: возможно return
//$1 - id, $2 - tourid, $3 winner id
pg_prepare($db_inst, QUERY_TOUR_SET_WINNER, 'update "Tournament" set "WinnerWizardId" = $3 where "Id" = $2 and "Owner" = $1 returning "WinnerWizardId"');

//$1 - id, $2 - tourid, $3 ex id
pg_prepare($db_inst, QUERY_TOUR_EXCLUDE, 'delete from "TournamentParticipant" tp where tp."WizardId" = $3 and tp."TournamentId" = $2 and EXISTS(select * from "Tournament" where "WinnerWizardId" is null and "Id" = $2 and "Owner" = $1) RETURNING *');

pg_prepare($db_inst, QUERY_GROUP_GET_INFO, 'select "Id" as "id", "Name" as "name", "AdminId" as "admin" from "Group" where "Id" = $1');
pg_prepare($db_inst, QUERY_GROUP_GET_STUDENTS, 'SELECT "Wizard"."Id" AS "id", "Wizard"."Name" as "name", "Wizard"."Age" as "age" FROM "Wizard" INNER JOIN "Group" ON "Wizard"."GroupId" = "Group"."Id" WHERE "Group"."Id" = $1 ORDER BY "Wizard"."Name"');
pg_prepare($db_inst, QUERY_GROUP_GET_LESSONS, 'select ls."Name" as "name", (select array_to_json(array_agg("Book"."Name")) as "books" from "Book" where "Book"."LessonId" = ls."Id"), (select array_to_json(array_agg("Wizard"."Name")) as "teachers" from "StudyPermission" inner join "Teacher" on "StudyPermission"."TeacherId" = "Teacher"."Id" inner join "Wizard" on "Wizard"."Id" = "Teacher"."WizardId" where "StudyPermission"."LessonId" = ls."Id") from "Group" gr inner join "Syllabus" sb On sb."GroupId" = gr."Id" inner join "Lesson" ls on ls."Id" = sb."LessonId" where gr."Id" = $1');

pg_prepare($db_inst, QUERY_ROOM_GET_INFO, 'SELECT "Name" as "name", "Id" as "id", (select count(*) from "Storage" where "RoomId" = "Room"."Id") as "numOfStorages" from "Room" where "Id" = $1');

pg_prepare($db_inst, QUERY_ROOM_GET_LIST, 'SELECT "Name" as "name", "Id" as "id", (select count(*) from "Storage" where "RoomId" = "Room"."Id") as "numOfStorages" from "Room"');

pg_prepare($db_inst, QUERY_STOR_GET_LIST, 'select "Storage"."Id" as "id", (select array_to_json(array_agg("Book"."Name")) as "books" from "Book" where "Book"."StorageId" = "Storage"."Id"), (select array_to_json(array_agg(json_build_object(\'id\', "Tube"."Id", \'name\',"Tube"."Name",\'potion\',"Potion"."Name"))) as "potions" from "Tube" inner join "Potion" on "Potion"."Id" = "Tube"."PotionId" where "Tube"."StorageId" = "Storage"."Id")  from "Storage" where "Storage"."RoomId" = $1');

function db_get_wizard_info($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_WIZARD, array($id));
	if(!$result)
		return -1;
	$ret = pg_fetch_array($result, null, PGSQL_ASSOC);
	pg_free_result($result);
	return $ret;
}
function db_get_wizard_books($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_WIZARD_BOOKS, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		$ret['authors'] = json_decode($ret['authors']);
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_room_list(){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_ROOM_GET_LIST, array());
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_room_info($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_ROOM_GET_INFO, array($id));
	$ret = pg_fetch_array($result, null, PGSQL_ASSOC);
	pg_free_result($result);
	return $ret;
}
function db_get_room_stors($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_STOR_GET_LIST, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		$ret['potions'] = json_decode($ret['potions']);
		if($ret['potions'][0] == null)
			$ret['potions'] = array();
		$ret['books'] = json_decode($ret['books']);
		if($ret['books'][0] == null)
			$ret['books'] = array();
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_wizard_skills($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_WIZARD_SKILLS, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		$ret['potions'] = json_decode($ret['potions']);
		if($ret['potions'][0] == null)
			$ret['potions'] = array();
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_group_info($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GROUP_GET_INFO, array($id));
	if(!$result)
		return -1;
	$ret = pg_fetch_array($result, null, PGSQL_ASSOC);
	pg_free_result($result);
	return $ret;
}
function db_get_group_students($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GROUP_GET_STUDENTS, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_group_lessons($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GROUP_GET_LESSONS, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		$ret['books'] = json_decode($ret['books']);
		if($ret['books'][0] == null)
			$ret['books'] = array();
		$ret['teachers'] = json_decode($ret['teachers']);
		if($ret['teachers'][0] == null)
			$ret['teachers'] = array();
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_wizard_tours($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_WIZARD_TOURS, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_wizard_lessons($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_WIZARD_LESSONS, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}

function db_tour_enter($id, $tourid){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_TOUR_ENTER, array($id, $tourid));
	$ret = pg_num_rows($result);
	pg_free_result($result);
	return $ret;
}
function db_tour_exit($id, $tourid){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_TOUR_EXIT, array($id, $tourid));
	$ret = pg_num_rows($result);
	pg_free_result($result);
	return $ret;
}
function db_tour_set_winner($id, $tourid, $winner){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_TOUR_SET_WINNER, array($id, $tourid, $winner));
	$ret = pg_num_rows($result);
	pg_free_result($result);
	return $ret;
}
function db_tour_exclude($id, $tourid, $excluded){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_TOUR_EXCLUDE, array($id, $tourid, $excluded));
	$ret = pg_num_rows($result);
	pg_free_result($result);
	return $ret;
}

function db_tour_add($id, $name, $lesson){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_TOUR_ADD, array($id, $name, $lesson));
	$ret = pg_num_rows($result);
	pg_free_result($result);
	return $ret;
}
function db_get_all_tours(){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_TOURS_LIST, array());
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}
function db_get_tour_info($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_TOUR_INFO, array($id));
	$ret = pg_fetch_array($result, null, PGSQL_ASSOC);
	pg_free_result($result);
	return $ret;
}
function db_get_tour_wizards($id){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_GET_TOUR_WIZARDS, array($id));
	$retarr = array();
	while($ret = pg_fetch_array($result, null, PGSQL_ASSOC)){
		array_push($retarr, $ret);
	}
	pg_free_result($result);
	return $retarr;
}


function db_check_login($login, $password){
	global $db_inst;
	$result = pg_execute($db_inst, QUERY_CHECK_LOGIN, array($login, $password));
	if(!$result)
		return -1;
	$ret = pg_fetch_array($result, null, PGSQL_ASSOC);
	pg_free_result($result);
	return $ret;
}

function db_close(){
	pg_close($db_inst);
}
//ssh -f -N -L 5432:pg:5432 s265097@cs.ifmo.ru -p 2222
//$db_inst->close();

?>
