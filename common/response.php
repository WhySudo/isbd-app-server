<?php
define("OK_CODE", 0);
define("ERROR_AUTHORIZED", 1);
define("ERROR_BAD_ARGUMENTS", 2);
define("ERROR_UNAUTHORIZED", 3);
define("ERROR_SQL", 4);
define("ERROR_AUTH_FAILED", 5);
function format_return_code($code, $info = ""){
	$ret = array("return_code"=>$code, "return_text"=>"OK");
	switch($code){
		case OK_CODE:
			$ret['return_text'] = "OK";
			break;
		case ERROR_AUTHORIZED:
			$ret['return_text'] = "You are already authorized";
			break;
		case ERROR_BAD_ARGUMENTS:
			$ret['return_text'] = "One or more arguments are not provided: ".$info;
			break;
		case ERROR_UNAUTHORIZED:
			$ret['return_text'] = "You are unauthorized";
			break;
		case ERROR_SQL:
			$ret['return_text'] = "SQL Error occuried";
			break;
		case ERROR_AUTH_FAILED:
			$ret['return_text'] = "Bad login or password, authorization failed";
			break;
		default:
			$ret['return_text'] = "Unknown error";
			break;
	}
	return real_json_encode($ret);
}
?>