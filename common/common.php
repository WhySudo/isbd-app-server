<?php
function check_param($name){
	if(!isset($_GET[$name])){
		die(format_return_code(ERROR_BAD_ARGUMENTS, $name));
	}
}
function real_json_encode($params){
	return json_encode($params, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
}
?>