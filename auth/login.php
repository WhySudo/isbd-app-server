<?php
//error_reporting(E_ALL);
//
include("../common/include.php");
if($yes_auth == 1){
	die(format_return_code(ERROR_AUTHORIZED));
}
check_param("login");
check_param("password");
$result = db_check_login( $_GET['login'],  $_GET['password']);
if($result == -1){
	die(format_return_code(ERROR_SQL));
}
else if(!$result){
	die(format_return_code(ERROR_AUTH_FAILED));
}
$_SESSION['auth_id'] = $result['id'];
die(real_json_encode($result));
/*else if($result == 0){
	die(format_return_code(ERROR_AUTH_FAILED));
}
$_SESSION['auth_id'] = $_GET['login'];
die(format_return_code(OK_CODE));
*/
?>