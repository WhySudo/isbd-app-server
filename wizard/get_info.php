<?php
//error_reporting(E_ALL);
//
include("../common/include.php");
if($yes_auth == 0){
	die(format_return_code(ERROR_UNAUTHORIZED));
}
check_param("id");
$result = db_get_wizard_info($_GET['id']);
if($result == -1){
	die(format_return_code(ERROR_SQL));
}
die(real_json_encode($result));
?>